import asyncio
from playwright.async_api import Playwright, async_playwright


async def save_html_pages(playwright: Playwright):
    # Запустить новый экземпляр браузера Chromium
    browser = await playwright.firefox.launch(headless=False)

    # Создать новую вкладку в браузере
    page = await browser.new_page()

    # Перейти на страницу входа
    await page.goto('https://www.cbr.ru/currency_base/daily/')
    path_bat = 'xpath=//html/body/main/div/div/div/div[3]/div/table/tbody/tr[35]/td[5]'
    path_turk = 'xpath=//html/body/main/div/div/div/div[3]/div/table/tbody/tr[36]/td[5]'
    path_kaz ='xpath=//html/body/main/div/div/div/div[3]/div/table/tbody/tr[20]/td[5]'

    locale_kaz = page.locator(path_kaz)
    locator_bat = page.locator(path_bat)
    locator_tur = page.locator(path_turk)

    exchange_three = await locale_kaz.inner_text()
    exchange_change_three = float(exchange_three.replace(",", ".")) / 10

    exchange = await locator_bat.inner_text()
    exchange_change = float(exchange.replace(",", ".")) / 10


    exchange_second = await locator_tur.inner_text()
    exchange_change_second = float(exchange_second.replace(",", ".")) / 10

    print(f"1 тенге лир стоит {exchange_change_three} рублей")
    print(f"1 тайский бат стоит {exchange_change} рублей")
    print(f"1 турецкий лир стоит {exchange_change_second} рублей")

    # Закрыть браузер
    await browser.close()


async def main():
    async with async_playwright() as playwright:
        await save_html_pages(playwright)


asyncio.run(main())
